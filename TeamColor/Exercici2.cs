﻿using System;

namespace TeamColor
{
    class Exercici2
    {
        static void Main()
        {
            Console.WriteLine("Introducir primer color y darle al enter, introducir el segundo color y darle al enter.");
            string color1 = Console.ReadLine();
            string color2 = Console.ReadLine();
            if (color1 == "white")
            {
                if (color2 == "green") Console.WriteLine("Team1");
                if (color2 == "blue") Console.WriteLine("Team2");
                if (color2 == "brown") Console.WriteLine("Team3");
            }else if (color1 == "red")
            {
                if (color2 == "blue") Console.WriteLine("Team4");
                if (color2 == "black") Console.WriteLine("Team5");
            }else if (color1 == "green")
            {
                if (color2 == "red") Console.WriteLine("Team6");
            }
            else
            {
                Console.WriteLine("ERROR");
            }
        }
    }
}
