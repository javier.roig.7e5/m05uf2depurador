﻿using System;

namespace Math1
{
    class Exercici3
    {
        static void Main()
        {
            var result = 0.0;
            for (int i = 0; i < 10000; i++)
            {
                if (result > 100)
                {
                    result = Math.Sqrt(result);
                }

                if (result < 0)
                {
                    result += result * result;
                }

                result += 20.2;
            }

            Console.WriteLine("El resultat és: " + $"{ result} ");

            //Quin valor té result quan la i == 1000?
            //30,762305898749055

            //El valor result és mai major que 110? I que 120?
            //Más de 110 si. Más de 120 no. El máximo que alcanza resultado es 111,56230589874906.
        }

    }
}
