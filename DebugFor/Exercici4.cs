﻿using System;

namespace DebugFor
{
    class Exercici4
    {
        static void Main()
        {
            UInt64 numero1 = 548745184;
            int numero2 = 25145;
            UInt64 result = 0;
            for (int i = 0; i < numero2; i++)
            {
                result += numero1;
            }

            Console.WriteLine("La multiplicació de {0} i {1} es {2}", numero1, numero2, result);
        }

        //El problema es que a partir de que la i tiene el valor 4 el resultado de la suma es mayor que el limite de tamaño de un int de 32 bits que es 2147483647.
        //La solución es cambiar el tipo de variable var/int a UInt64 que es un int pero de 64 bits.

    }
}
