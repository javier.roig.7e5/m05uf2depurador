﻿using System;

namespace IsAdult
{
    public class Exercici1
    {
        static Boolean isAdult(int todayYear, int todayMonth, int todayDay, int birthYear,
            int birthMonth, int birthDay)
        {
            //ORIGINAL
            //var isAdult = todayYear > birthYear + 18;
            //var isDifficultYear = todayYear == birthYear + 18;
            //var isAdultForMonth = isDifficultYear && todayMonth >= birthMonth;
            //var isDifficultMonth = isDifficultYear && todayMonth == birthMonth;
            //var isAdultForDay = isDifficultMonth && todayDay > birthDay;

            //MODIFICADO
            var isAdult = todayYear - birthYear >= 18;
            var isDifficultYear = todayYear - birthYear < 18;
            var isAdultForMonth = isDifficultYear && todayMonth >= birthMonth;
            var isDifficultMonth = isDifficultYear && todayMonth < birthMonth;
            var isAdultForDay = isDifficultMonth && todayDay > birthDay;
            Console.WriteLine("isAdult: " + isAdult);
            return isAdult || isAdultForMonth || isAdultForDay;
        }

        static void Main()
        {
            if (!isAdult(2018, 10, 17, 1985, 1, 1))//33 años 9 meses 16 dias
            {
                Console.WriteLine("Ha fallat: isAdult(2018, 10, 17, 1985, 1, 1)");
            }
            if (isAdult(2018, 10, 17, 2015, 1, 1))//3 años 9 meses 16 dias
            {
                Console.WriteLine("Ha fallat isAdult(2018, 10, 17, 2015, 1, 1)");
            }
            if (!isAdult(2018, 10, 17, 2000, 1, 1))//18 años 9 meses 16 dias
            {
                Console.WriteLine("Ha fallat: isAdult(2018, 10, 17, 2000, 1, 1)");
            }
            if (isAdult(2018, 10, 17, 2000, 12, 1))//17 años 10 meses 16 dias
            {
                Console.WriteLine("Ha fallat isAdult(2018, 10, 17, 2000, 12, 1)");
            }
            if (!isAdult(2018, 10, 17, 2000, 10, 1))//18 años 0 meses 16 dias
            {
                Console.WriteLine("Ha fallat: isAdult(2018, 10, 17, 2000, 10, 1)");
            }
            if (!isAdult(2018, 10, 17, 2000, 10, 17))//18 años 0 meses 0 dias
            {
                Console.WriteLine("Ha fallat: isAdult(2018, 10, 17, 2000, 10, 17)");
            }
            if (isAdult(2018, 10, 17, 2000, 10, 30))//17 años 11 meses 17 dias
            {
                Console.WriteLine("Ha fallat isAdult(2018, 10, 17, 2000, 10, 30)");
            }
        }
    }
}
